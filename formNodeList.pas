unit formNodeList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, formMain;

type
  TfrmNodeList = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    lbNodes: TListBox;
    procedure lbNodesDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ShowListResult: TTreeNode;
    function ShowList(Nodes: TTreeNodeArray): boolean;
  end;

var
  frmNodeList: TfrmNodeList;

implementation

uses
  Parser,
  Utils;

{$R *.dfm}

{ TfrmNodeList }

function TfrmNodeList.ShowList(Nodes: TTreeNodeArray): boolean;
var i: integer;
begin
  lbNodes.Clear;
  for i:=0 to Length(Nodes)-1 do
    if (Nodes[i] <> nil) and (Nodes[i].Data <> nil) and (TObject(Nodes[i].Data) is TParseItem) then
    begin
      lbNodes.AddItem(GetParentName(Nodes[i]) + (TObject(Nodes[i].Data) as TParseItem).name, Nodes[i]);
    end;
  if ShowModal() = mrOK then
  begin
    if lbNodes.ItemIndex = -1 then
    begin
      ShowListResult := nil;
      Result := false;
    end else
    begin
      ShowListResult := lbNodes.Items.Objects[lbNodes.ItemIndex] as TTreeNode;
      Result := true;
    end;
  end else
    Result := false;
end;

procedure TfrmNodeList.lbNodesDblClick(Sender: TObject);
begin
  Close();
  ModalResult := mrOK;
end;

end.

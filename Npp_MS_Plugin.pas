unit Npp_MS_Plugin;

interface

uses
  NppPlugin, SysUtils, Windows, Classes, nppp_scifuncs, SciSupport, formMain;

const
  // dialog ID
  DLGID_MAIN_FORM = 1;

  // The WM_UNICHAR message is the same as WM_CHAR, except it uses UTF-32
  WM_UNICHAR = $0109;

type TWndProc=function (hWnd: HWND; Msg: UINT;  wParam: WPARAM;  lParam: LPARAM): LRESULT; stdcall;

type
  TNppPlugin_MS = class(TNppPlugin)
  public
    InitOk: boolean;
    PathToMS: WideString;
    InTree_DblClick: Byte;
    cmdStructWindow: DWORD;
    cmdOption: DWORD;
    needLoadData: boolean;

    // hook:
    ScintillaMainWndProc: TWndProc;
    ScintillaSecondWndProc: TWndProc;

    SCI: TNPPSciFunctions;

    constructor Create;
    procedure Init; virtual;
    procedure HookWndProc;

    function GetConfig(ParamName: WideString; Default: string = ''): WideString;
    procedure SetConfig(ParamName: WideString; Text: WideString);

    procedure LoadConfig; virtual;
    procedure SaveConfig; virtual;

    procedure FuncOptions;
    procedure FuncStructTextDocking;
    procedure DoNppnToolbarModification; override;
    procedure BeNotified(sn: PSCNotification); override;

    procedure OnAfterScMsg(hWnd: HWND; Msg: UINT; W: WPARAM; L: LPARAM);

    procedure OnMouseWheel();
    procedure OnClick(ClickType: integer);
    procedure OnClickWithCtrl;
    procedure OnDblClick;
    procedure OnKeyPress(Key: WideChar);
    procedure OnKeyUp(Key: Word; Shift: TShiftState);
  end;

procedure _FuncOptions; cdecl;
procedure _FuncStructTextDocking; cdecl;
Procedure Log(s:string);

function ScintillaMainNewWndProc(hWnd: HWND; Msg: UINT;  W: WPARAM;  L: LPARAM): LRESULT; stdcall;
function ScintillaSecondNewWndProc(hWnd: HWND; Msg: UINT;  W: WPARAM;  L: LPARAM): LRESULT; stdcall;

var
  // NotepadPP Plugin
  Nppp: TNppPlugin_MS;

  //Note: just point to Nppp (see in "initialization"). Need for compatible with 'kernel' code.
  Npp:  TNppPlugin;

implementation

uses
  Messages,
  Parser,
  Forms, // for KeyDataToShiftState
  Utils,
  ComCtrls,
  Math,
  StrUtils,
  Dialogs,
  Controls,
  formOptions,
  formNodeList,
  nppp_consts;

function ScintillaMainNewWndProc(hWnd: HWND; Msg: UINT;  W: WPARAM;  L: LPARAM): LRESULT; stdcall;
begin
  Result:=Nppp.ScintillaMainWndProc(hWnd,Msg,W,L);
  Nppp.OnAfterScMsg(hWnd,Msg,W,L);
end;

function ScintillaSecondNewWndProc(hWnd: HWND; Msg: UINT;  W: WPARAM;  L: LPARAM): LRESULT; stdcall;
begin
  Result:=Nppp.ScintillaSecondWndProc(hWnd,Msg,W,L);
  Nppp.OnAfterScMsg(hWnd,Msg,W,L);
end;


constructor TNppPlugin_MS.Create;
//var sk: TShortcutKey;
begin
  inherited;

  InitOk := false;
  self.PathToMS := '';
  self.PluginName := '&Max Script';

  {
  // CTRL ALT SHIFT F7
  sk.IsCtrl := true;
  sk.IsAlt := true;
  sk.IsShift := false;
  sk.Key := #118;
  }

  self.AddFuncItem('Show window', _FuncStructTextDocking);
  self.AddFuncItem('Options', _FuncOptions);

  // CTRL SHIFT SPACE
  {sk.IsCtrl := true;
  sk.IsAlt := false;
  sk.IsShift := false;
  sk.Key := VK_SPACE;
  self.AddFuncItem('Show fn help (in editor)', _FuncShowEditHint, sk);
  }

  //self.AddFuncItem('-', nil);
  //self.AddFuncItem('About', _FuncAbout);
end;

procedure _FuncShowEditHint; cdecl;
begin
  // Nppp.ShowEditHint();
end;

procedure _FuncOptions; cdecl;
begin
  Nppp.FuncOptions;
end;

procedure _FuncStructTextDocking; cdecl;
begin
  Nppp.FuncStructTextDocking;
end;

procedure TNppPlugin_MS.FuncOptions;
begin
  if InitOk = false then exit;
  if frmOptions=nil then
    frmOptions := TfrmOptions.Create(nil);

  // options to form
  frmOptions.edPathToMS.Text := PathToMS;
  frmOptions.cbTree_DblClick.ItemIndex := InTree_DblClick;

  if frmOptions.ShowModal() = mrOK then
  begin
    // options from form
    with frmOptions.edPathToMS do
    begin
      if DirectoryExists(Text) then
      begin
        PathToMS := Text;
        frmMain.ReloadTreeData;
      end else
        ShowMessage('Folder not found. ('+Text+')');
    end;
    InTree_DblClick := frmOptions.cbTree_DblClick.ItemIndex;
    SaveConfig;
  end;
end;

procedure TNppPlugin_MS.FuncStructTextDocking;
begin
  if (Assigned(frmMain)) then
    if frmMain.Visible then
      frmMain.Hide else
      frmMain.Show;
end;

procedure TNppPlugin_MS.DoNppnToolbarModification;
var
  tb: TToolbarIcons;
begin
  tb.ToolbarIcon := 0;
  // �������� ������ ��������. �� NPP �������� �� ���������� ��.
  //tb.ToolbarIcon := LoadIcon(Hinstance, 'IDB_TB_STXT_PANEL');
  //tb.ToolbarBmp := 0;

  // �������� ��������� ������ (��� ������������)
  tb.ToolbarBmp := LoadImage(Hinstance, 'IDB_TB_ICON_PANEL', IMAGE_BITMAP, 0, 0, (LR_DEFAULTSIZE or LR_LOADMAP3DCOLORS));
  SendMessage(self.NppData.NppHandle, NPPM_ADDTOOLBARICON, WPARAM(cmdStructWindow), LPARAM(@tb));
end;

Procedure Log(s:string);
Begin
  {if Nppp.InitOk then
    if frmMain<>nil then
      frmMain.lbDebug.Items.Insert(0, s);
  }
enD;

procedure TNppPlugin_MS.BeNotified(sn: PSCNotification);
var
  s: WideString;
begin
  inherited;
  //if (HWND(sn^.nmhdr.hwndFrom) = self.NppData.NppHandle) then
  begin
    case (sn^.nmhdr.code) of
    NPPN_READY: begin
      self.Init;
    end;
    NPPN_FILEOPENED: begin
      //Note: NPPN_FILEOPENED useless!
      // see https://github.com/notepad-plus-plus/notepad-plus-plus/issues/252
    end;
    // ����������� ������������ �������
    NPPN_BUFFERACTIVATED: begin
      if frmMain <> nil then
        frmMain.TimerShowEditHint.Enabled:=false;
      s := GetString(HWND(sn^.nmhdr.hwndFrom) {self.NppData.NppHandle}, NPPM_GETFILENAME);
      s := Trim(s);
      if RightStr(s, 3) = '.ms' then
        if needLoadData = false then
          if frmMain <> nil then
            if not frmMain.Visible then
            begin
              frmMain.ReloadTreeData;
              frmMain.Show;
            end else
              // if visible - do nothing
          else
            needLoadData := true;
    end;
    NPPN_FILECLOSED: begin
      //if (frmMain <> nil) then Log('NPPN_FILECLOSED: '+ ' ID:'+IntToStr(sn.nmhdr.idFrom));
    end;

    else
      //StructTextDockingForm.lbDebug.Items.Add(IntToStr(sn^.nmhdr.code));
    end;
  end;
end;

procedure TNppPlugin_MS.Init;
begin
  LoadConfig;

  SCI := TNPPSciFunctions.Create;
  SCI.setNPPHandle(Npp.NppData.NppHandle);
  SCI.setSci1Handle(Npp.NppData.ScintillaMainHandle);
  SCI.setSci2Handle(Npp.NppData.ScintillaSecondHandle);
  
  if (not Assigned(frmMain)) then
  begin
    frmMain := TfrmMain.Create(self, DLGID_MAIN_FORM);
    if needLoadData then
    begin
      frmMain.ReloadTreeData;
      frmMain.Show;
    end else
      frmMain.Hide;
  end;
  cmdStructWindow := self.FuncArray[0].CmdID;
  cmdOption := self.FuncArray[1].CmdID;

  if (not Assigned(frmOptions)) then
    frmOptions := TfrmOptions.Create(nil);
  frmOptions.Hide;

  if (not Assigned(frmNodeList)) then
    frmNodeList := TfrmNodeList.Create(nil);
  frmNodeList.Hide;

  // hook Scintilla "window proc".
  //todo: unset hook before destroy?
  Nppp.HookWndProc;

  // ���������� ����� � ��������
  SetFocus(Self.NppData.ScintillaMainHandle);

  InitOk := true;
end;

function TNppPlugin_MS.GetConfig;
begin
  SetLength(Result, 4000);
  Result[1]:=#0;
  GetPrivateProfileStringW(
    'Config',
    PWideChar(ParamName),
    '',
    @Result[1],
    Length(Result)-1,
    PWideChar(Nppp.GetPluginsConfigDir+'\'+'MaxScript.ini'));
  SetLength(Result, Pos(#0, Result)-1);
end;

procedure TNppPlugin_MS.SetConfig;
begin
  WritePrivateProfileStringW(
    'Config',
    PWideChar(ParamName),
    PWideChar(Text),
    PWideChar(Nppp.GetPluginsConfigDir+'\'+'MaxScript.ini'))
end;

procedure TNppPlugin_MS.LoadConfig;
begin
  PathToMS := NormalizePath(GetConfig('Path', ''));
  InTree_DblClick := StrToIntDef(GetConfig('InTree_DblClick', '0'), 0);
  //Note: frmOptions.pathList loading in "TfrmOptions.FormShow"
end;

procedure TNppPlugin_MS.SaveConfig;
var
  i: integer;
begin
  SetConfig('Path', PathToMS);
  SetConfig('InTree_DblClick', IntToStr(InTree_DblClick));

  for i:=1 to Min(frmOptions.pathList.Count, 10) do
    SetConfig('Path'+IntToStr(i), frmOptions.pathList[i-1]);
end;

procedure TNppPlugin_MS.HookWndProc;
begin
  ScintillaMainWndProc:=TWndProc(SetWindowLongW(Npp.NppData.ScintillaMainHandle, GWL_WNDPROC, integer(@ScintillaMainNewWndProc)));
  ScintillaSecondWndProc:=TWndProc(SetWindowLongW(Npp.NppData.ScintillaSecondHandle, GWL_WNDPROC, integer(@ScintillaSecondNewWndProc)));
end;

procedure TNppPlugin_MS.OnAfterScMsg(hWnd: HWND; Msg: UINT; W: WPARAM;
  L: LPARAM);
begin
  if (Msg = WM_MOUSEWHEEL) then
  begin
    OnMouseWheel();
  end;
  if (Msg >= WM_MOUSEFIRST) and (Msg <= WM_MOUSELAST) then
  begin
    if Msg <> WM_MOUSEMOVE then
      OnClick(Msg);
  end;
  if (Msg = WM_LBUTTONUP) and ((W and MK_CONTROL) <> 0) then
  begin
    OnClickWithCtrl();
  end;
  if (Msg = WM_LBUTTONDBLCLK) then
  begin
    OnDblClick();
  end;
  { not work.
  if (Msg = WM_UNICHAR) then
  begin
    OnKeyPress(WideChar(W));
  end;}
  if (Msg = WM_CHAR) then
  begin
    OnKeyPress(WideChar(W));
  end;
  if (Msg = WM_KEYUP) then
  begin
    OnKeyUp(W, KeyDataToShiftState(L));
  end;
end;

procedure TNppPlugin_MS.OnClickWithCtrl;
var
  s, s2: nppString;
  nds: TTreeNodeArray;
begin
  s := SCI.getLine(SCI.getCurrentLine());
  s := GetNameId(s, SCI.getCurrentColumn()-2);
  s2 := Npp.GetString(NppData.NppHandle, NPPM_GETCURRENTWORD, 256);

  if frmMain.FindObjsByName(s, nds) then
  begin
    if Length(nds) = 1 then
      frmMain.OpenObject(nds[0]) else
      if frmNodeList.ShowList(nds) then
        frmMain.OpenObject(frmNodeList.ShowListResult);
  end else
  begin
    if frmMain.FindObjsByName(s2, nds) then
    begin
      if Length(nds) = 1 then
        frmMain.OpenObject(nds[0]) else
        if frmNodeList.ShowList(nds) then
          frmMain.OpenObject(frmNodeList.ShowListResult);
    end;
  end;
end;

procedure TNppPlugin_MS.OnDblClick;
var
  s, s2: nppString;
  //OLD: f: TTreeNode;
  nds: TTreeNodeArray;
begin
  s := SCI.getLine(SCI.getCurrentLine());
  s := GetNameId(s, SCI.getCurrentColumn()-2);
  s2 := Npp.GetString(NppData.NppHandle, NPPM_GETCURRENTWORD, 256);
  if frmMain.FindObjsByName(s, nds) then
  begin
    if Length(nds) = 1 then
      frmMain.UpdateHintBar(nds[0]) else
      frmMain.UpdateHintBarArray(nds);
  end else
  begin
    if frmMain.FindObjsByName(s2, nds) then
    begin
      if Length(nds) = 1 then
        frmMain.UpdateHintBar(nds[0]) else
        frmMain.UpdateHintBarArray(nds);
    end;
    //OLD: if frmMain.FindObjByName(s2, f) then frmMain.UpdateHintBar(f);
  end;
end;

procedure TNppPlugin_MS.OnKeyPress;
begin
  if Key=' ' then
  begin
    // restart timer
    frmMain.TimerShowEditHint.Enabled:=false;
    if frmMain.NeedShowEditHint() then
      frmMain.TimerShowEditHint.Enabled:=true;
  end else
  begin
    frmMain.EditHint.ReleaseHandle();
    frmMain.TimerShowEditHint.Enabled:=false;
  end;
end;

procedure TNppPlugin_MS.OnClick(ClickType: integer);
begin
  frmMain.EditHint.ReleaseHandle();
  frmMain.TimerShowEditHint.Enabled:=false;
end;

procedure TNppPlugin_MS.OnMouseWheel;
begin
  frmMain.EditHint.ReleaseHandle();
  frmMain.TimerShowEditHint.Enabled:=false;
end;

procedure TNppPlugin_MS.OnKeyUp(Key: Word; Shift: TShiftState);
begin
  if (Key = VK_SPACE) and ([ssCtrl, ssShift] = Shift) then
    frmMain.ShowEditHint() else
    begin
      if (Key <> VK_SHIFT) and (Key <> VK_CONTROL) then
      begin
        frmMain.EditHint.ReleaseHandle();
        frmMain.TimerShowEditHint.Enabled:=false;
      end;
    end;
end;

initialization
  Nppp := TNppPlugin_MS.Create;
  Npp := Nppp;
end.

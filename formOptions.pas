unit formOptions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls;

type
  TfrmOptions = class(TForm)
    ScrollBox1: TScrollBox;
    btnSelectDir: TButton;
    btnCancel: TBitBtn;
    btnOk: TBitBtn;
    lbPath: TEdit;
    chOnlyName: TCheckBox;
    chInsertStructName: TCheckBox;
    edPathToMS: TComboBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    chShowWhenOpenMS: TCheckBox;
    btnClearPaths: TButton;
    cbTree_DblClick: TComboBox;
    lbTree_DblClick: TEdit;
    CheckBox3: TCheckBox;
    procedure btnSelectDirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnClearPathsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    oldPath: WideString;
    pathList: TStringList;
  end;

var
  frmOptions: TfrmOptions;

implementation

uses Npp_MS_Plugin, FileCtrl, Utils;

{$R *.dfm}

procedure TfrmOptions.btnSelectDirClick(Sender: TObject);
var d: string;
begin
  d := edPathToMS.Text;
  if SelectDirectory('�������� �������', '', d) then
    edPathToMS.Text := NormalizePath(d);
end;

procedure TfrmOptions.FormShow(Sender: TObject);
var
  i: integer;
  s: string;
begin
  oldPath := edPathToMS.Text;
  if pathList = nil then
    pathList := TStringList.Create;
  pathList.Clear;
  pathList.Duplicates := dupIgnore;
  for i:=1 to 10 do
  begin
    s := Nppp.GetConfig('Path'+IntToStr(i), '');
    if (s<>'') then
      if pathList.IndexOf(s)=-1 then
        pathList.Add(s);
  end;
  edPathToMS.Items.Assign(pathList);
end;

procedure TfrmOptions.btnOkClick(Sender: TObject);
begin
  edPathToMS.Text := NormalizePath(edPathToMS.Text);
  if oldPath <> edPathToMS.Text then
    pathList.Insert(0, oldPath);
end;

procedure TfrmOptions.btnClearPathsClick(Sender: TObject);
var
  i: integer;
begin
  pathList.Clear;
  edPathToMS.Items.Clear;
  for i:=1 to 10 do
    Nppp.SetConfig('Path'+IntToStr(i), '');
end;

end.

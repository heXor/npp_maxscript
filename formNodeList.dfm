object frmNodeList: TfrmNodeList
  Left = 277
  Top = 149
  Width = 371
  Height = 326
  Caption = 'List:'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 240
    Width = 353
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      353
      41)
    object BitBtn1: TBitBtn
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 270
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object lbNodes: TListBox
    Left = 8
    Top = 8
    Width = 336
    Height = 220
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 16
    TabOrder = 0
    OnDblClick = lbNodesDblClick
  end
end

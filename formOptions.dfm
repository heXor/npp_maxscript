object frmOptions: TfrmOptions
  Left = 333
  Top = 158
  Width = 387
  Height = 345
  AutoSize = True
  Caption = 'Options'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 0
    Width = 369
    Height = 300
    TabOrder = 0
    object btnSelectDir: TButton
      Left = 330
      Top = 23
      Width = 24
      Height = 25
      Caption = '...'
      TabOrder = 0
      OnClick = btnSelectDirClick
    end
    object btnCancel: TBitBtn
      Left = 267
      Top = 258
      Width = 92
      Height = 31
      TabOrder = 1
      Kind = bkCancel
    end
    object btnOk: TBitBtn
      Left = 11
      Top = 258
      Width = 92
      Height = 31
      TabOrder = 2
      OnClick = btnOkClick
      Kind = bkOK
    end
    object lbPath: TEdit
      Left = 4
      Top = 5
      Width = 352
      Height = 18
      AutoSelect = False
      AutoSize = False
      BorderStyle = bsNone
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 3
      Text = 'Path to 3D Max Script:'
    end
    object chOnlyName: TCheckBox
      Left = 6
      Top = 59
      Width = 350
      Height = 21
      Caption = 'Show function param in list'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 4
    end
    object chInsertStructName: TCheckBox
      Left = 6
      Top = 85
      Width = 350
      Height = 21
      Caption = 'Insert struct name'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 5
    end
    object edPathToMS: TComboBox
      Left = 6
      Top = 23
      Width = 320
      Height = 24
      ItemHeight = 16
      TabOrder = 6
    end
    object CheckBox1: TCheckBox
      Left = 6
      Top = 111
      Width = 350
      Height = 21
      Caption = 'Load sources from edit file dir'
      Enabled = False
      TabOrder = 7
    end
    object CheckBox2: TCheckBox
      Left = 6
      Top = 137
      Width = 350
      Height = 21
      Caption = 'Show current file in tree'
      Enabled = False
      TabOrder = 8
    end
    object chShowWhenOpenMS: TCheckBox
      Left = 6
      Top = 162
      Width = 350
      Height = 21
      Caption = 'Show when open MS file'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 9
    end
    object btnClearPaths: TButton
      Left = 266
      Top = 59
      Width = 92
      Height = 31
      Caption = 'Clear paths'
      TabOrder = 10
      OnClick = btnClearPathsClick
    end
    object cbTree_DblClick: TComboBox
      Left = 152
      Top = 192
      Width = 209
      Height = 24
      Style = csDropDownList
      ItemHeight = 16
      ItemIndex = 0
      TabOrder = 11
      Text = 'Open source'
      Items.Strings = (
        'Open source'
        'Insert func. name to text')
    end
    object lbTree_DblClick: TEdit
      Left = 2
      Top = 195
      Width = 149
      Height = 18
      AutoSelect = False
      AutoSize = False
      BorderStyle = bsNone
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 12
      Text = 'On dbl. click in tree:'
    end
    object CheckBox3: TCheckBox
      Left = 6
      Top = 220
      Width = 350
      Height = 21
      Caption = 'Show func. help on CTRL+SHIFT+SPACE'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 13
    end
  end
end

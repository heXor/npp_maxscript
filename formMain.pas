unit formMain;

interface

{$DEFINE HintShadowDisable}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,

  NppDockingForms,
  NppPlugin,
  ExtCtrls, Menus, ComCtrls, ImgList, Buttons;

type
  // Fix hint drop shadow error in Win7 ("frezze shadow")
  TXPHintWindow = class(THintWindow)
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure WMNCPaint(var msg: TMessage); message WM_NCPAINT;
  end;

  TTreeNodeArray = array of TTreeNode;

type
  TfrmMain = class(TNppDockingForm)
    Panel1: TPanel;
    Splitter1: TSplitter;
    PopupMenu1: TPopupMenu;
    mnInsert: TMenuItem;
    mnShowSrc: TMenuItem;
    ImageList1: TImageList;
    mnCopyText: TMenuItem;
    Comment1: TMenuItem;
    btnReload: TBitBtn;
    treeMain: TTreeView;
    TimerUpdateHint: TTimer;
    TimerHideHint: TTimer;
    btnOptions: TBitBtn;
    edFind: TEdit;
    btnFind: TButton;
    panelHelp: TPanel;
    memoComment: TMemo;
    imageItem: TImage;
    lbItemName: TLabel;
    TimerShowEditHint: TTimer;
    btnReloadCurrent: TBitBtn;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);

    procedure FormHide(Sender: TObject);
    procedure FormFloat(Sender: TObject);
    procedure FormDock(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mnInsertClick(Sender: TObject);

    procedure treeMainDeletion(Sender: TObject; Node: TTreeNode);
    procedure treeMainKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure treeMainDblClick(Sender: TObject);
    procedure treeMainMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure treeMainMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure treeMainMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

    procedure btnReloadClick(Sender: TObject);
    procedure btnReloadCurrentClick(Sender: TObject);
    procedure mnShowSrcClick(Sender: TObject);
    procedure mnCopyTextClick(Sender: TObject);
    procedure Comment1Click(Sender: TObject);
    procedure TimerUpdateHintTimer(Sender: TObject);
    procedure TimerHideHintTimer(Sender: TObject);
    procedure btnOptionsClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure TimerShowEditHintTimer(Sender: TObject);
  public
    Visible: boolean;

    // for click in TreeView and process in menu
    LastX,
    LastY: integer;
    LastNode: TTreeNode;
    CurrentFileNode: TTreeNode;

    SpecialHint: TXPHintWindow;
    EditHint: TXPHintWindow;

    procedure CreateSpecialHint; virtual;

    function FindObjByName(ObjName: nppString; var finded: TTreeNode): boolean;
    function FindObjsByName(FullObjName: nppString; var FindList: TTreeNodeArray): boolean;
    procedure OpenSelectedObject;
    procedure OpenObject(Obj: TTreeNode);
    procedure OpenObjectByName(ObjName: nppString);
    procedure InsertObject(obj: TTreeNode);
    // ���� ������ ��� ���� (������ - ��� ��� ���� "����")
    function GetFileRootFromNode(n: TTreeNode; var root: TTreeNode): boolean;

    procedure UpdateHint();
    procedure UpdateHintBar(Obj: TTreeNode);
    procedure UpdateHintBarArray(Objs: TTreeNodeArray);
    procedure ReloadTreeData();
    procedure ReloadCurrentTabData(OpenTree: boolean = false);
    procedure ShowEditHint();
    function NeedShowEditHint(): boolean;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  Parser,
  Utils,
  Npp_MS_Plugin,
  Clipbrd,
  NppForms,
  SCISupport, Types;

{$R *.dfm}

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  self.NppDefaultDockingMask := DWS_DF_CONT_LEFT;//DWS_DF_FLOATING; // whats the default docking position
  self.KeyPreview := true; // special hack for input forms
  self.OnFloat := self.FormFloat;
  self.OnDock := self.FormDock;

  CreateSpecialHint;
  inherited;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  inherited;
  //
end;

// special hack for input forms
// This is the best possible hack I could came up for
// memo boxes that don't process enter keys for reasons
// too complicated... Has something to do with Dialog Messages
// I sends a Ctrl+Enter in place of Enter
procedure TfrmMain.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  //if (Key = #13) and (self.Memo1.Focused) then self.Memo1.Perform(WM_CHAR, 10, 0);
end;

// Docking code calls this when the form is hidden by either "x" or self.Hide
procedure TfrmMain.FormHide(Sender: TObject);
begin
  inherited;
  Visible := false;
  Nppp.SetMenuItemCheck(Nppp.cmdStructWindow, false);
end;

procedure TfrmMain.FormDock(Sender: TObject);
begin
  Nppp.SetMenuItemCheck(Nppp.cmdStructWindow, true);
end;

procedure TfrmMain.FormFloat(Sender: TObject);
begin
  Nppp.SetMenuItemCheck(Nppp.cmdStructWindow, true);
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  inherited;
  Visible := true;
  Nppp.SetMenuItemCheck(Nppp.cmdStructWindow, true);
end;

procedure TfrmMain.btnReloadClick(Sender: TObject);
begin
  ReloadTreeData();
end;

procedure TfrmMain.btnReloadCurrentClick(Sender: TObject);
begin
  ReloadCurrentTabData(true);
end;

procedure TfrmMain.treeMainDeletion(Sender: TObject; Node: TTreeNode);
begin
  inherited;
  TObject(Node.Data).Free();
  //todo: bug - AV  =(
  //if Node.Data <> nil then (TObject(Node.Data) as TParseItem).Free();
end;

procedure TfrmMain.treeMainMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if (Button = mbLeft) then
  begin
    UpdateHintBar(treeMain.Selected);
    if (Shift = [ssShift]) then
      OpenSelectedObject else
      if (Shift = [ssCtrl]) then
        InsertObject(treeMain.Selected);
  end;
end;

procedure TfrmMain.treeMainMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if treeMain.GetNodeAt(X,Y) <> nil then
    treeMain.Selected := treeMain.GetNodeAt(X,Y);
end;

procedure TfrmMain.treeMainKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  UpdateHintBar(treeMain.Selected);
  if Key = VK_RETURN then
    OpenSelectedObject;
end;

procedure TfrmMain.mnInsertClick(Sender: TObject);
begin
  inherited;
  InsertObject(treeMain.Selected);
  //InsertObject(treeMain.GetNodeAt(LastX,LastY));
end;

procedure TfrmMain.mnShowSrcClick(Sender: TObject);
begin
  inherited;
  OpenSelectedObject;
end;

procedure TfrmMain.treeMainMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  LastX := X;
  LastY := Y;
  TimerUpdateHint.Enabled := true;
end;

procedure TfrmMain.treeMainDblClick(Sender: TObject);
begin
  inherited;
  case Nppp.InTree_DblClick of
  0: OpenSelectedObject();
  1: InsertObject(treeMain.Selected);
  else
     OpenSelectedObject();
  end;
end;

procedure TfrmMain.mnCopyTextClick(Sender: TObject);
begin
  inherited;
  if treeMain.Selected <> nil then
    clipboard.AsText := treeMain.Selected.Text;
end;

procedure TfrmMain.UpdateHint;
var
  r:TRect;
  h, w:integer;
begin
  if LastNode = nil then
  begin
    SpecialHint.ReleaseHandle;
  end else
    if (LastNode.Data <> nil) and (TObject(LastNode.Data) is TParseItem) then
      if (TParseItem(LastNode.Data).comment = '') then
        SpecialHint.ReleaseHandle else
        begin
          // calculate Hint posiotion
          r := LastNode.DisplayRect(false);
          //test: w := r.Right - r.Left;
          h := r.Bottom - r.Top;
          w := treeMain.ClientWidth;
          r.Right := r.Left + w;
          r.Top := r.Top + h;
          r.Bottom := r.Bottom + h;
          r.TopLeft := treeMain.ClientToScreen(r.TopLeft);
          r.BottomRight := treeMain.ClientToScreen(r.BottomRight);

          // calc rect (using DT_CALCRECT)
          // http://stackoverflow.com/questions/7719025/calculating-size-of-text-before-drawing-to-a-canvas
          DrawTextW(Canvas.Handle,
            PWideChar(TParseItem(LastNode.Data).comment),
            Length(TParseItem(LastNode.Data).comment),
            r,
            DT_LEFT or DT_WORDBREAK or DT_CALCRECT);
          r.Bottom := r.Bottom + 10;
          w := treeMain.ClientWidth;
          r.Right := r.Left + w;

          SpecialHint.ActivateHint(r, TParseItem(LastNode.Data).comment);
          SpecialHint.Show;
        end;
end;

procedure TfrmMain.Comment1Click(Sender: TObject);
begin
  inherited;
  if treeMain.Selected <> nil then
    if (treeMain.Selected.Data <> nil) and (TObject(treeMain.Selected.Data) is TParseItem) then
      if (TObject(treeMain.Selected.Data) as TParseItem).comment <> '' then
        ShowMessage((TObject(treeMain.Selected.Data) as TParseItem).comment);
end;

procedure TfrmMain.UpdateHintBar(Obj: TTreeNode);
var t: WideString;
begin
  if (Obj <> nil) and
   (Obj.Data <> nil) and (TObject(Obj.Data) is TParseItem)
   and (TParseItem(Obj.Data).comment <> '') then
    begin
      //todo: image assign work, but only once  =(
      //if Obj.ImageIndex > 0 then
      //  ImageList1.GetBitmap(Obj.ImageIndex, imageItem.Picture.Bitmap);
      t := '';
      // add name and owner name
      t := t + GetParentName(Obj);
      t := t + TParseItem(Obj.Data).name;
      lbItemName.Caption := t;
      memoComment.Text := TParseItem(Obj.Data).comment;
      panelHelp.Visible := true;
    end else
      panelHelp.Visible := false;
end;

function IsWinXP: Boolean;
begin
  Result := (Win32Platform = VER_PLATFORM_WIN32_NT) and
    (Win32MajorVersion >= 5) and (Win32MinorVersion >= 1);
end;

procedure TXPHintWindow.CreateParams(var Params: TCreateParams);
const
  CS_DROPSHADOW = $00020000;
begin
  inherited;
{$IFDEF HintShadowDisable}
  Params.WindowClass.Style := Params.WindowClass.Style xor (Params.WindowClass.Style and CS_DROPSHADOW);
{$ELSE}
  if IsWinXP then
    Params.WindowClass.Style := Params.WindowClass.Style or CS_DROPSHADOW;
{$ENDIF}
end;

procedure TXPHintWindow.WMNCPaint(var msg: TMessage);
{$IFNDEF HintShadowDisable}
var
  R: TRect;
  DC: HDC;
{$ENDIF}
begin
  inherited;
{$IFNDEF HintShadowDisable}
  DC := GetWindowDC(Handle);
  try
    R := Rect(0, 0, Width, Height);
    DrawEdge(DC, R, EDGE_ETCHED, BF_RECT or BF_MONO);
  finally
    ReleaseDC(Handle, DC);
  end;
{$ENDIF}
end;

///////////////

procedure TfrmMain.TimerUpdateHintTimer(Sender: TObject);
begin
  inherited;
  LastNode := treeMain.GetNodeAt(LastX,LastY);
  TimerUpdateHint.Enabled := false;
  TimerHideHint.Enabled := true;
  UpdateHint;
end;

procedure TfrmMain.TimerHideHintTimer(Sender: TObject);
var r: TRect;
begin
  if SpecialHint <> nil then
  begin
    r := treeMain.ClientRect;
    r.TopLeft := treeMain.ClientToScreen(r.TopLeft);
    r.BottomRight := treeMain.ClientToScreen(r.BottomRight);
    if not PtInRect(r, Mouse.CursorPos) then
    begin
      SpecialHint.ReleaseHandle;
      TimerHideHint.Enabled := false;
    end;
  end;
end;

procedure TfrmMain.CreateSpecialHint;
begin
  SpecialHint := TXPHintWindow.Create(self);
  EditHint := TXPHintWindow.Create(self);
end;

procedure TfrmMain.btnOptionsClick(Sender: TObject);
begin
  inherited;
  Nppp.FuncOptions;
end;

procedure ParseText(n: TTreeNode; strs: TStrings);
var
  i, i2: integer;
  p: TParse;
  s: WideString;
begin
  p := TParse.Create;
  // init parser
  p.Init;
  p.root := n;
  p.branch := n;
  // enum all string
  for i:=0 to strs.Count-1 do
  begin
    s:=strs[i];
    p.line := i;
    // enum all char of string
    for i2:=1 to Length(s) do
    begin
      p.ParseChar(s[i2]);
    end;
    p.ParseChar(#13);
  end;
  //todo: check parse valid
  p.ParseEnd;
  p.Free;
end;

procedure TfrmMain.ReloadCurrentTabData;
var
  strs: TStrings;
begin
  if CurrentFileNode <> nil then
  begin
    treeMain.Items.BeginUpdate;
    CurrentFileNode.DeleteChildren();
    CurrentFileNode.Expanded := false;
    strs := TStringList.Create;
    strs.Clear;
    // load data from editor
    strs.Text := Nppp.SCI.getText();
    // and parse text
    ParseText(CurrentFileNode, strs);
    strs.Free;
    if OpenTree then
      CurrentFileNode.Expand(true);
    treeMain.Items.EndUpdate;
  end;
end;

procedure TfrmMain.ReloadTreeData;
var
  n, n2: TTreeNode;
  strs: TStrings;
var
  fpath, fname: WideString;
  fd: TWin32FindDataW;
  fh: THandle;
  i:integer;
  s:string;
begin

  if CurrentFileNode = nil then
  begin
    // add current file node
    n := treeMain.Items.Add(nil, 'Current');
    n.ImageIndex := imgFile;
    n.Data := TParseItem.Create(stsFile);
    TParseItem(n.Data).name := 'Current';
    CurrentFileNode := n;
  end;

  // remove all nodes, except 'Current' node
  n := treeMain.Items.GetFirstNode();
  if n <> nil then
    repeat
      n2 := n.getNextSibling();
      if (n <> CurrentFileNode) then
        n.Delete;
      n := n2;
    until n = nil;

  treeMain.Items.BeginUpdate;
  strs := TStringList.Create;

  // update tree
  ReloadCurrentTabData();

  fpath := TNppPlugin_MS(Npp).PathToMS;
  fh := FindFirstFileW(PWideChar(fpath + '*.ms'), fd);
  if fh <> INVALID_HANDLE_VALUE then
  begin
    // enum all files
    repeat
      // create node (type of "file") in tree (in root)
      fname := fd.cFileName;
      n := treeMain.Items.Add(nil, fname);
      n.ImageIndex := imgFile;
      n.Data := TParseItem.Create(stsFile);
      TParseItem(n.Data).name := fd.cFileName;

      // load data from file
      strs.Clear;
      strs.LoadFromFile(fpath+fname);

      ParseText(n, strs);

      n.Expand(false);
    until not FindNextFileW(fh, fd);
  end;

  treeMain.Items.EndUpdate;
  strs.Free;
  windows.FindClose(fh);
end;

procedure TfrmMain.InsertObject;
var
  sw: WideString;
  s: string;
  n: TTreeNode;
  p: TParseItem;
  len: integer;
begin
  n := obj;
  if n <> nil then
  begin
    p := (TObject(n.Data) as TParseItem);
    if (p.ItemType = stsFile) then
    begin
      sw := TNppPlugin_MS(Npp).PathToMS + n.Text;
      if FileExists(sw) then
        Npp.DoOpen(sw);
    end else
    if (p.ItemType = stsFunction) or (p.itemType = stsStruct) or (p.itemType = stsStructValue) then
    begin
      //ToDo: options: insert struct name

      if (p.itemType = stsFunction) and (n.Parent.Data <> nil) and
        ((TObject(n.Parent.Data) as TParseItem).itemType = stsStruct)
      then
        s := ((TObject(n.Parent.Data) as TParseItem).name) + '.' + Trim(p.rawText)
      else
      if (p.itemType = stsFunction) then
        s := Trim(p.rawText)
      else
      if (p.itemType = stsStruct) then
        s := Trim(p.name)
      else
      if (p.itemType = stsStructValue) then
        s := ((TObject(n.Parent.Data) as TParseItem).name) + '.' + Trim(p.name);

      //ToDo: ��� ������� �����, ����� �������� � ������, �� ��� ������� ������ ����� ��� �� ����� ������� �������� �����
      len := Nppp.SCI.getParam(SCI_GETCURRENTPOS) + Length(s);
      SendMessage(Npp.NppData.ScintillaMainHandle, SCI_INSERTTEXT, -1, LPARAM(PChar(s)));
      //Npp.DoOpen(Npp.GetString(Npp.NppData.NppHandle, NPPM_GETFULLCURRENTPATH), SCI.getCurrentLine);
      //Npp.GotoLine(SCI.getCurrentLine);
      Nppp.SCI.setParam(SCI_SETEMPTYSELECTION, len);
    end else
  end;
end;

procedure TfrmMain.OpenSelectedObject;
begin
  OpenObject(treeMain.Selected);
end;

procedure TfrmMain.OpenObject(Obj: TTreeNode);
var
  root: TTreeNode;
  t: nppString;
begin
  // ����� ��������� ����
  if (Obj <> nil) and (Obj.Data <> nil) then
  begin
    // ���� ������ - ��� ��������� ��� �����
    if GetFileRootFromNode(Obj, root) then
    begin
      root.Expand(false);
      treeMain.Selected := Obj;
      // ��������� ���� � ��������� � ������������ ������ ����� �����
      t := TNppPlugin_MS(Npp).PathToMS + root.Text;
      if FileExists(t) then
        Npp.DoOpen(t, TParseItem(Obj.Data).line - 1);
    end;
  end;
end;

function TfrmMain.GetFileRootFromNode(n: TTreeNode; var root: TTreeNode): boolean;
begin
  root := nil;
  Result := false;
  while (true) do
  begin
    if (n = nil) or (n.Data = nil) or (not (TObject(n.Data) is TParseItem)) then
      break;
    if TParseItem(n.Data).itemType = stsFile then
    begin
      root := n;
      Result := true;
      break;
    end;
    n := n.GetPrev;
  end;
end;

procedure TfrmMain.OpenObjectByName(ObjName: nppString);
var finded: TTreeNode;
begin
  if FindObjByName(ObjName, finded) then
    OpenObject(finded);
end;

function TfrmMain.FindObjByName(ObjName: nppString;
  var finded: TTreeNode): boolean;
  {OLD: function RecurFind(Obj: TTreeNode; ObjName: nppString; var finded: TTreeNode): boolean;
  var i: integer;
  begin
    result := false;
    if Obj = nil then exit;
    if (Obj.Data <> nil) and ((TObject(Obj.Data) as TParseItem).name = ObjName) then
    begin
      finded := Obj;
      result := true;
      exit;
    end;
    for i:=0 to Obj.Count-1 do
      if RecurFind(Obj.Item[i], ObjName, finded) then
      begin
        result := true;
        exit;
      end;
  end;}
var
  CurItem: TTreeNode;
begin
  result := false;
  
  //finded := nil;
  //Note: recursion work. but this call not work =)
  //      i hate delphi TTreeView! he dont have _really_ root node!
  //Result := RecurFind(treeMain.Items.GetFirstNode(), ObjName, finded);

  CurItem := treeMain.Items.GetFirstNode;
  while CurItem <> nil do
  begin
    if (CurItem.Data <> nil) and ((TObject(CurItem.Data) as TParseItem).name = ObjName) then
    begin
      finded := CurItem;
      result := true;
      exit;
    end;
    CurItem := CurItem.GetNext;
  end;
end;

procedure TfrmMain.btnFindClick(Sender: TObject);
begin
  OpenObjectByName(edFind.Text);
end;

function TfrmMain.FindObjsByName(FullObjName: nppString; var FindList: TTreeNodeArray): boolean;
  function GetNodeName(Node: TTreeNode; WithParent: bool): WideString;
  begin
    Result := '';
    if (Node.Data <> nil) then
    begin
      if not WithParent then
        Result := (TObject(Node.Data) as TParseItem).name else
        if (Node.Parent <> nil) and (Node.Parent.Data <> nil) then
          Result := (TObject(Node.Parent.Data) as TParseItem).name + '.' + (TObject(Node.Data) as TParseItem).name;
    end;
  end;
var
  HaveDot: boolean;
  CurItem: TTreeNode;
begin
  SetLength(FindList, 0);
  HaveDot := pos('.', FullObjName) <> 0;

  // enum all items
  CurItem := treeMain.Items.GetFirstNode;
  while CurItem <> nil do
  begin
    if GetNodeName(CurItem, HaveDot) = FullObjName then
    begin
      // add finded item to array
      SetLength(FindList, Length(FindList)+1);
      FindList[Length(FindList)-1] := CurItem;
    end;
    CurItem := CurItem.GetNext;
  end;

  Result := Length(FindList) > 0;
end;

procedure TfrmMain.UpdateHintBarArray(Objs: TTreeNodeArray);
var
  t: WideString;
begin
  lbItemName.Caption := 'Finded '+IntToStr(Length(Objs))+':';
  t := CollectHelpTextFromNodes(Objs);
  memoComment.Text := t;
  panelHelp.Visible := true;
end;

procedure TfrmMain.TimerShowEditHintTimer(Sender: TObject);
begin
  TimerShowEditHint.Enabled:=false;
  ShowEditHint();
end;

procedure TfrmMain.ShowEditHint;
var
  str, s: nppString;
  p: TPoint;
  r: TRect;
  nds: TTreeNodeArray;
begin
  str := Nppp.SCI.getLine(Nppp.SCI.getCurrentLine());
  // magic const =)
  s := GetNameId(str, Nppp.SCI.getCurrentColumn()-2);

  if not FindObjsByName(s, nds) then
  begin
    s := GetNameId(str, Nppp.SCI.getCurrentColumn()-2, false);
    if not FindObjsByName(s, nds) then
    begin
      EditHint.ReleaseHandle();
      exit;
    end;
  end;

  s := CollectFuncAttrHelpTextFromNodes(nds);

  // calculate Hint posiotion
  r := Rect(0,0,0,0);
  // calc rect (using DT_CALCRECT)
  // http://stackoverflow.com/questions/7719025/calculating-size-of-text-before-drawing-to-a-canvas
  DrawTextW(EditHint.Canvas.Handle,
    PWideChar(s),
    Length(s),
    r,
    DT_LEFT {or DT_WORDBREAK} or DT_CALCRECT);
  p := Nppp.SCI.GetCaretScreenLocation();
  p.Y := p.Y + Nppp.SCI.TextHeight(Nppp.SCI.getCurrentLine());
  r.Left := p.X;
  r.Top := p.Y;
  r.Right := r.Right + p.X + 16;
  r.Bottom := r.Bottom + p.Y;

  EditHint.ActivateHint(r, s);
end;

function TfrmMain.NeedShowEditHint(): boolean;
var
  str: nppString;
  l, c: integer;
begin
  //todo: "tabulation" problem?
  str := RTrimAll(Nppp.SCI.getLine(Nppp.SCI.getCurrentLine()));
  l := Length(str);
  c := Nppp.SCI.getCurrentColumn();
  // ShowMessage(IntToStr(c)+' '+IntToStr(l));
  // magic const =)
  if c+4 >= l then
    Result := true else
    Result := false;
end;

end.

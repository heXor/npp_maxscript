library Npp_MaxScript;

{$R 'Npp_res.res' 'Npp_res.rc'}

uses
  SysUtils,
  Classes,
  Types,
  Windows,
  Messages,
  NppPlugin,
  SCISupport,
  NppForms,
  NppDockingForms in 'lib\NppDockingForms.pas' {NppDockingForms},
  Npp_MS_Plugin in 'Npp_MS_Plugin.pas',
  formMain in 'formMain.pas' {PluginDockingForm},
  formOptions in 'formOptions.pas' {frmOptions},
  Utils in 'Utils.pas',
  Parser in 'Parser.pas',
  formNodeList in 'formNodeList.pas' {frmNodeList};

{$R *.res}

{$Include 'lib\NppPluginInclude.pas'}

begin
  { First, assign the procedure to the DLLProc variable }
  DllProc := @DLLEntryPoint;
  { Now invoke the procedure to reflect that the DLL is attaching to the process }
  DLLEntryPoint(DLL_PROCESS_ATTACH);
end.

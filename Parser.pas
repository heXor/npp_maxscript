unit Parser;

interface

uses
  ComCtrls;

//ToDo: WIP!!! �� ����������� "/* */" ������������ "/" - ����� ��������� ��������� ��� ������ ������
//ToDo: WIP!!! � ������ "xxx" ����������� "xxx - ����� ��������� ��������� ��� ����� ������ ������

{
/[*](.*)[*]/
--(.*)
(
)
"(.*)"
@"(.*)"
[a-z0-9_]+
.*


�������� �������:
����� ���������� �� ����������� �� ������ - ����� � ����� �������� ���.
  � ����� �������� ����� ���������� ��� ���� "�����".
� ������ ���������� ����� ���������� "������".

fn x s() = x
fn x s() = (
)
fn x s() = x = x

fn x e = e.e = if e.d.c() then ppp else ppp

� ��� ��� ����� ��������:
fn GetMeshSelection node meshClass = if canconvertto node meshClass do undo off
	 (
		local panelMode = getCommandPanelTaskMode()
		disableRefMsgs()
    ...

}

type
  TParserState = (
    stNone,
    stCommentZone,
    stCommentLine,
    stStr,
    stStrEasy,
    stSpaces,
    stNextLine,
    stOpenBlock,
    stCloseBlock,
    stAssign,
    stId);

  TStructState = (
    stsNone,
    stsFile, // ���� - � ������� �� ������������, ������������ � ������
    stsFunction, // ������ ����������� ������� �� 'fn' � �� '='
    stsFunctionAfterAssign, // ����� �����, ��� ������ ��� ���
    stsFunctionWaitEndLine, // ��������� ������������ �������, ���� ���������� ������
    stsStruct, // ������ ����������� ��������� �� 'struct' �� '('
    stsStructWaitValue, // ����� ����������� � ������ ������� ����������
    stsStructValue // ���� ����� �������� �������� ����������. �� '=' �� ',' ��� ')'
  );

  TParseItem = class
  public
    itemType: TStructState;
    name: WideString;
    rawText: WideString;
    comment: WideString;
    line: integer;
    balanceLevel: integer;
    extInfo: array of variant;
    constructor Create(initItemType: TStructState);
    destructor Free;
  end;

  TParse = class
  public
    prevC: WideChar;

    substStrSkip: boolean;
    oldState, state: TParserState;
    balance, line: integer;

    text,
    lastComment: WideString;
    lastCommentEmptyLine: integer;

    structState: TStructState;

    root,
    branch: TTreeNode;
    tempItem: TParseItem;

    // temp string - optimization. (for fast work - this placed here).
    tt: WideString;

    procedure Init;
    // ��������� ��������
    procedure ParseChar(c: WideChar); virtual;
    procedure ParseLexem(st: TParserState; t: WideString); virtual;
    function ParseEnd: boolean; virtual;

    // ������� ��� ��������
    function AddChild(node: TTreeNode; text: WideString): TTreeNode; virtual;
    procedure DetectFunction(st: TParserState; OneLine: boolean); virtual;
    procedure DetectStruct(st: TParserState); virtual;
    procedure DetectValue(st: TParserState); virtual;
  end;

const
  imgFunction = 1;
  imgStruct = 2;
  imgValue = 3;
  imgFile = 4;

implementation

uses
  formMain,
  Npp_MS_Plugin,
  SysUtils;

{ TParse }

procedure TParse.ParseChar(c: WideChar);
var
  update: boolean;
  skipChar: boolean;
begin
  if c = #10 then exit;
  oldState := state;
  update := false;
  skipChar := false;

  // exit to stNone. (�������� �� case ��������� ��� ������� � stNone)
  case state of
    stId:
      if not (((UpperCase(c) >= 'A') and (UpperCase(c) <= 'Z')) or
         ((c >= '0') and (c <= '9')) or (c = '_')) then
        state := stNone;
    // try end state - in next case reset this state
    stAssign, stNextLine, stSpaces, stOpenBlock, stCloseBlock:
      state := stNone;
  end;

  case state of
    stNone: begin
      if (c = '(') then begin
        state := stOpenBlock;
        inc(balance);
      end else
      if (c = ')') then begin
        state := stCloseBlock;
        dec(balance);
      end else
      if (c = #13) then begin
        state := stNextLine;
        update := true;
      end else
      if (c = ' ') or (c = #09) then
        state := stSpaces else
      if (c = '=') then
        state := stAssign else
      if (prevC = '/') and (c = '*') then
        state := stCommentZone else
      if (prevC = '-') and (c = '-') then
        state := stCommentLine else
      if (prevC = '@') and (c = '"') then
        state := stStrEasy else
      if c = '"' then
        state := stStr else
      if ((UpperCase(c) >= 'A') and (UpperCase(c) <= 'Z')) or
         ((c >= '0') and (c <= '9')) or
         (c = '_') then
        state := stId;
    end;

    stCommentZone: // /* ... */
      if (prevC = '*') and (c = '/') then
        state := stNone;

    stCommentLine: // // ... #13
      if c = #13 then
        state := stNone;

    stStr:
      if substStrSkip then
        substStrSkip := false {and skip this char} else
      if c = '"' then begin
        state := stNone;
        skipChar := true;
      end
      else
      if c = '\' then
        substStrSkip := true;

    stStrEasy:
      if c = '"' then begin
        state := stNone;
        skipChar := true;
      end;

  end;

  if ((state <> oldState) or (update)) and (text<>'') then
  begin
    // ������� ������ �������
    if oldState = stCommentLine then
      Delete(text, 1, 1);
    if oldState = stStr then
      Delete(text, 1, 1);
    if oldState = stStrEasy then
      Delete(text, 1, 1);
    if oldState = stCommentZone then begin
      Delete(text, Length(text), 1);
      Delete(text, 1, 1);
    end;

    ParseLexem(oldState, text);
    text := '';
  end;
  if not skipChar then
    text := text + c;

  prevC := c;
end;

//todo: TMP, OLD
procedure WriteFL(FileName,str:string; ReWriteFile:boolean=false);
var f:text;
begin
  assign(f,FileName);
  if FileExists(FileName) and not ReWriteFile then
    append(f) else
    rewrite(f);
  write(f,str);
  close(f);
end;
function spaces(x: byte): string;
var i: integer;
begin
  result := '';
  for i:=1 to x do result := result + '  ';
end;

function clearComment(s: WideString): string;
var
  I, L: Integer;
begin
  L := Length(S);
  I := 1;
  while (I <= L) and ((S[I] <= ' ') or (S[I] = '/') or (S[I] = '-')) do Inc(I);
  if I > L then Result := '' else
  begin
    while ((S[L] <= ' ') or (S[L] = '/') or (S[L] = '-')) do Dec(L);
    Result := Copy(S, I, L - I + 1);
  end;
end;

procedure TParse.ParseLexem(st: TParserState; t: WideString);
begin
  tt:=LowerCase(t);

  // ���������� �����������
  if (st=stCommentZone) or (st=stCommentLine) then
  begin
    lastCommentEmptyLine := 0;
    lastComment := clearComment(t);
  end;
  // ���������� ����������� ���� ���� ������
  if (st=stOpenBlock) or (st=stCloseBlock) then
    lastComment := '';
  // ���������� ����������� ���� ���� ��������� ��������
  if (st=stNextLine) then
    inc(lastCommentEmptyLine);
  if (lastComment <> '') and (lastCommentEmptyLine > 1) then
    lastComment := '';

  // ���� ���� ���� �� ����������� � ��� ����� �����
  if (tempItem <> nil) then
    if (st = stNone) or (st = stId) or (st = stSpaces) then
      tempItem.rawText := tempItem.rawText + t;

  // ������� � ����������� � ����������� ����������� �� �����
  if (st = stSpaces) or (st=stCommentZone) or (st=stCommentLine) then
    exit;

  // lexical analysis - build a tree of lexem // ����������� ������ - ������ ������ ������
  case structState of
    stsNone, stsStructWaitValue:
    begin
      // await "variable" // ����� �������� ����������
      if (structState = stsStructWaitValue) then
        if (st = stAssign) then
        begin
          // '=' ����� ������������ - ������ ��� ����������
          structState := stsStructValue;
        end else begin
          // ���� ��� '=' ����� ���������� �������� � ����������� ������
          structState := stsNone;
          FreeAndNil(tempItem);
        end;

      if st = stId then
      begin
        // FUNCTION:
        if (tt='fn') or (tt='function') then
        begin
          structState := stsFunction;
          tempItem := TParseItem.Create(stsFunction);
          tempItem.comment := lastComment;
        end else
        // STRUCT:
        if (tt='struct') then
        begin
          structState := stsStruct;
          tempItem := TParseItem.Create(stsStruct);
          tempItem.comment := lastComment;
        end else
        // VALUE:
        if (TObject(branch.Data) as TParseItem).itemType = stsStruct then
        begin
          // ����������� � ��������� - ������ ����� ����������
          structState := stsStructValue;
          tempItem := TParseItem.Create(stsStructValue);
          tempItem.name := t;
          tempItem.rawText := t;
          tempItem.comment := lastComment;
        end;
      end;
    end;
    stsFunction:
    begin
      begin
        if st = stId then
        begin
          if tempItem.name = '' then
            tempItem.name := t;
        end;
        // ��������� '=' - ���� ��� �������� ������ ��� �����������
        if (st = stAssign) then
          structState := stsFunctionAfterAssign;
      end;
    end;
    stsFunctionAfterAssign:
    begin
      if (st = stOpenBlock) then
      begin
        structState := stsNone;
        DetectFunction(st, false);
      end else
      if (st = stId) then
      begin
        structState := stsFunctionWaitEndLine;
        DetectFunction(st, true);
      end;
      // default:
      //structState := stsNone;
    end;
    stsFunctionWaitEndLine:
    begin
      {if (st = stNone) and (t = ',') then
        structState := stsNone;}
      if st = stNextLine then
        structState := stsNone;
      //if st = stNextLine then
      //  structState := stsFunctionAfterEndLine;
      // default:
      //structState := none change
    end;
    stsStruct:
    begin
      if st = stId then
      begin
        if tempItem.name = '' then
          tempItem.name := t;
      end else
      if st = stOpenBlock then
      begin
        structState := stsNone;
        DetectStruct(st);
      end;
    end;
    stsStructValue:
    begin
      if ((st = stNone) and (t=',')) or (st = stCloseBlock) then
      begin
        structState := stsNone;
        DetectValue(st);
      end
    end;
  end;

  // ���������� ���������� ������ - ������������ ���������� �����.
  // � �������������� ������� �� ��������� ��� ��������� (��� ������ �������).
  if st = stCloseBlock then
  begin
    if (TObject(branch.Data) as TParseItem).balanceLevel > balance then
      if branch <> root then
      begin
        // ���� ������� ������� ������ ���� ������ �������� ������� ����� �� �������.
        branch := branch.Parent;
        structState := stsNone;
        // ���� ��� ����������� ������� item �� ������� ��� - �� ����������
        if tempItem <> nil then
          FreeAndNil(tempItem);
      end;
  end;
end;

procedure TParse.Init;
begin
  oldState:=stNone;
  state:=stNone;
  structState:=stsNone;
  balance:=0;
  text:='';
  lastComment:='';
  substStrSkip:=false;
  prevC := #0;
end;

function TParse.ParseEnd: boolean;
begin
  Result := (balance = 0) and ((state = stNone) or (state = stNextLine)) and (structState = stsNone);
end;

function TParse.AddChild(node: TTreeNode; text: WideString): TTreeNode;
begin
  result := frmMain.treeMain.Items.AddChild(node, Trim(text));
end;

procedure TParse.DetectFunction;
var n: TTreeNode;
begin
  // ���������� Function
  tempItem.balanceLevel := balance;
  tempItem.line := line;

  //todo: options - function with param or only name
  //n := AddChild(branch, tempItem.name);
  n := AddChild(branch, tempItem.rawText);
  n.ImageIndex := imgFunction;
  n.Data := tempItem;
  tempItem := nil;

  // �� ������������ ������� - ������ � ����
  if (not OneLine) then
    branch := n;
end;

procedure TParse.DetectStruct(st: TParserState);
var n: TTreeNode;
begin
  // ���������� Struct
  tempItem.balanceLevel := balance;
  tempItem.line := line;

  n := AddChild(branch, tempItem.name);
  n.ImageIndex := imgStruct;
  n.Data := tempItem;
  tempItem := nil;
  // ������ � ����
  branch := n;
end;

procedure TParse.DetectValue(st: TParserState);
var n: TTreeNode;
begin
  // ���������� Function
  tempItem.balanceLevel := balance;
  tempItem.line := line + 1;

  n := AddChild(branch, tempItem.name);
  n.ImageIndex := imgValue;
  n.Data := tempItem;
  tempItem := nil;
end;

{ TParseItem }

constructor TParseItem.Create(initItemType: TStructState);
begin
  inherited Create;
  itemType := initItemType;
  SetLength(extInfo, 0);
end;

destructor TParseItem.Free;
begin
  SetLength(extInfo, 0);
  inherited Free;
end;

end.

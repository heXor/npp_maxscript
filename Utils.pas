unit Utils;

interface

uses
  Types,
  SysUtils,
  Parser,
  formMain,
  ComCtrls;

function GetNameId(S: WideString; Pos: integer; UseDot: boolean = true): WideString;

function NormalizePath(path: WideString): WideString;

Function RTrim(s: WideString): WideString;

Function RTrimAll(s: WideString): WideString;

// Tree View Utils:

function GetParentName(N: TTreeNode): WideString;

function CollectHelpTextFromNodes(Objs: TTreeNodeArray): WideString;

function CollectFuncAttrHelpTextFromNodes(Objs: TTreeNodeArray): WideString;

implementation

Function RTrim(s: WideString): WideString;
begin
  Result := s;
  while (Result <> '') and ((Result[length(Result)] = ' ') or (Result[length(Result)] = #$09)) do
    Delete(Result, length(Result), 1);
End;

Function RTrimAll(s: WideString): WideString;
begin
  Result := s;
  while (Result <> '') and (Result[length(Result)] <= #32) do
    Delete(Result, length(Result), 1);
End;

function CollectFuncAttrHelpTextFromNodes(Objs: TTreeNodeArray): WideString;
var
  t: WideString;
  i: integer;
begin
  t := '';
  for i:=0 to Length(Objs)-1 do
  begin
    if (Objs[i] <> nil) and
     (Objs[i].Data <> nil) and (TObject(Objs[i].Data) is TParseItem) then
      begin
        // add name and owner name
        t := t + GetParentName(Objs[i]);
        t := t + TParseItem(Objs[i].Data).name; // + sLineBreak;
        t := t + ' ';
        t := t + Trim(TParseItem(Objs[i].Data).rawText);
        if i < Length(Objs)-1 then t := t + sLineBreak;
      end;
  end;
  Result := t;
end;

function CollectHelpTextFromNodes(Objs: TTreeNodeArray): WideString;
var
  t: WideString;
  i: integer;
begin
  t := '';
  for i:=0 to Length(Objs)-1 do
  begin
    if (Objs[i] <> nil) and
     (Objs[i].Data <> nil) and (TObject(Objs[i].Data) is TParseItem) then
      begin
        // add name and owner name
        t := t + GetParentName(Objs[i]);
        t := t + TParseItem(Objs[i].Data).name + sLineBreak;
        if (TParseItem(Objs[i].Data).comment <> '') then
          t := t + TParseItem(Objs[i].Data).comment;
        if i < Length(Objs)-1 then
          t := t + sLineBreak + '--------' + sLineBreak;
      end;
  end;
  Result := t;
end;

function GetParentName(N: TTreeNode): WideString;
begin
  Result := '';
  // get parent name
  if (N <> nil) and (N.Parent <> nil) and
    (N.Parent.Data <> nil) and (TObject(N.Parent.Data) is TParseItem) then
  begin
    if TParseItem(N.Parent.Data).itemType = stsFile then
     Result := TParseItem(N.Parent.Data).name + ': ' else
     Result := TParseItem(N.Parent.Data).name + '.';
  end;
end;

function GetNameId(S: WideString; Pos: integer; UseDot: boolean = true): WideString;
  function GoodChar(c: WideChar): boolean;
  begin
    Result := (((UpperCase(c) >= 'A') and (UpperCase(c) <= 'Z')) or
       ((c >= '0') and (c <= '9')) or (c = '_') or (UseDot and (c = '.')));
  end;
var
  i, i2: integer;
begin
  for i:=Pos downto 1 do
    if not GoodChar(S[i]) then break;
  Inc(i);
  for i2:=Pos to Length(S) do
    if not GoodChar(S[i2]) then break;
  Dec(i2);
  if i = i2 then
    Result := '' else
    Result := Copy(S, i, i2-i+1);
end;

function NormalizePath(path: WideString): WideString;
begin
  Result := path;
  if Length(path) > 1 then
    if path[Length(path)] <> PathDelim then
      Result := path + PathDelim;
end;

end.

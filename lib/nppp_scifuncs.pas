{
    Delphi Foundation for creating plugins for Notepad++
    (Short: DFPN++)

    Copyright (C) 2009 Bastian Blumentritt

    This file is part of DFPN++.

    DFPN++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DFPN++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DFPN++.  If not, see <http://www.gnu.org/licenses/>.
}

unit nppp_scifuncs;

interface

uses
  Windows,
  nppp_types;

type
  TNPPSciFunctions = class(TObject)
  private
    FNPPHandle: HWND;
    FSci1Handle: HWND;
    FSci2Handle: HWND;
    FSciCurrentHandle: HWND;
    FNestedUpdateCalls: Cardinal;
  public
    constructor Create;
    procedure beginUpdate;
    procedure endUpdate;
    procedure setNPPHandle(pHandle: HWND);
    procedure setSci1Handle(pHandle: HWND);
    procedure setSci2Handle(pHandle: HWND);
    function getCurrentSciHandle: HWND;
    function getNppHandle: HWND;

    // Line position of the cursor in the editor
    function getCurrentLine: Integer;

    // Column position of the cursor in the editor
    function getCurrentColumn: Integer;

    function getLineCount: Integer;
    function getSelectionStart: Integer;
    function getSelectionEnd: Integer;
    function getSelectedText: nppString;
    function getCurrentPos: integer;

    function GetCaretScreenLocation: TPoint;

    /// returns the complete document content as string.
    function getText: nppString;

    /// returns the text of line (lines start at 0)
    function getLine(Line: integer): nppString;

    {
     This removes any selection and sets the caret at the start of line
     number /pLineNo/ and scrolls the view (if needed) to make it visible.
     The anchor position is set the same as the current position. If /pLineNo/
     is outside the lines in the document (first line is 0), the line set is
     the first or last.
    }
    procedure goToLine(pLineNo: Integer);

    function TextHeight(line: integer): integer;

    procedure replaceSelection(const pWith: nppString);
    procedure setSelectionEnd(pPos: Integer);
    procedure setSelectionStart(pPos: Integer);

    function getParam(ParamMSG: integer; Value1: integer = 0; Value2: integer = 0): integer;
    function setParam(ParamMSG: integer; Value1: integer = 0; Value2: integer = 0): integer;
  end;

implementation

uses
  Classes,
  nppp_consts;

{ TNPPBaseFunctions }

constructor TNPPSciFunctions.Create;
begin
  inherited;
  FNPPHandle := 0;
  FSci1Handle := 0;
  FSci2Handle := 0;
  FSciCurrentHandle := 0;
  FNestedUpdateCalls := 0;
end;

procedure TNPPSciFunctions.setNPPHandle(pHandle: HWND);
begin
  FNPPHandle := pHandle;
end;

procedure TNPPSciFunctions.setSci1Handle(pHandle: HWND);
begin
  FSci1Handle := pHandle;
end;

procedure TNPPSciFunctions.setSci2Handle(pHandle: HWND);
begin
  FSci2Handle := pHandle;
end;

function TNPPSciFunctions.getCurrentSciHandle: HWND;
var
  tP: PINT;
  i: Integer;
begin
  if FSciCurrentHandle > 0 then
    Result := FSciCurrentHandle
  else
  begin
    tP := @i;

    SendMessage(FNPPHandle, NPPM_GETCURRENTSCINTILLA, 0, LPARAM(tP));
    if tP^ = 0 then
      Result := FSci1Handle
    else
      Result := FSci2Handle;
  end;
end;

procedure TNPPSciFunctions.beginUpdate;
begin
  Inc(FNestedUpdateCalls);
  if (FSciCurrentHandle = 0) then
    FSciCurrentHandle := getCurrentSciHandle;
end;

procedure TNPPSciFunctions.endUpdate;
begin
  if FNestedUpdateCalls <= 1 then
    FSciCurrentHandle := 0;
  if FNestedUpdateCalls > 0 then
    Dec(FNestedUpdateCalls);
end;

function TNPPSciFunctions.getCurrentLine: Integer;
//var r: Integer;
begin
  // http://stackoverflow.com/questions/20974827/getting-current-cursor-pos-within-notepad-plugin
  Result := SendMessage(getNppHandle, NPPM_GETCURRENTLINE, 0, 0);

  {OLD Code:
  beginUpdate;
  r := SendMessage(getCurrentSciHandle, SCI_GETCURRENTPOS, 0, 0);
  Result := SendMessage(getCurrentSciHandle, SCI_LINEFROMPOSITION, r, 0);
  endUpdate;}
end;

function TNPPSciFunctions.getCurrentColumn: Integer;
begin
  Result := SendMessage(getNppHandle, NPPM_GETCURRENTCOLUMN, 0, 0);
end;


function TNPPSciFunctions.getSelectedText: nppString;
var
  tR: Integer;
  tS: String;
begin
  beginUpdate;
  // determine selection length first
  // -> correctly returns selection length+1
  tR := SendMessage(getCurrentSciHandle, SCI_GETSELTEXT, 0, 0);
  SetLength(tS, tR);
  SendMessage(getCurrentSciHandle, SCI_GETSELTEXT, 0, LPARAM(PChar(tS)));

  Result := PChar(tS);

  endUpdate;
end;

function TNPPSciFunctions.getSelectionEnd: Integer;
begin
  Result := SendMessage(getCurrentSciHandle, SCI_GETSELECTIONEND, 0, 0);
end;

function TNPPSciFunctions.getSelectionStart: Integer;
begin
  Result := SendMessage(getCurrentSciHandle, SCI_GETSELECTIONSTART, 0, 0);
end;

function TNPPSciFunctions.getText: nppString;
var
  tR: Integer;
  tS: String;
begin
  beginUpdate;
  tR := SendMessage(getCurrentSciHandle, SCI_GETTEXTLENGTH, 0, 0);
  Inc(tR);
  SetLength(tS, tR);
  SendMessage(getCurrentSciHandle, SCI_GETTEXT, tR, LPARAM(PChar(tS)));

  Result := tS;

  endUpdate;
end;

function TNPPSciFunctions.getLine(Line: integer): nppString;
var
  tR: Integer;
  tS: String;
begin
  beginUpdate;
  tR := SendMessage(getCurrentSciHandle, SCI_LINELENGTH, Line, 0);
  Inc(tR);
  SetLength(tS, tR);
  SendMessage(getCurrentSciHandle, SCI_GETLINE, Line, LPARAM(PChar(tS)));

  Result := tS;

  endUpdate;
end;

procedure TNPPSciFunctions.goToLine(pLineNo: Integer);
begin
  SendMessage(getCurrentSciHandle, SCI_GOTOLINE, pLineNo, 0);
end;

procedure TNPPSciFunctions.replaceSelection(const pWith: nppString);
begin
  SendMessage(getCurrentSciHandle, SCI_REPLACESEL, 0, LPARAM(PChar(String(pWith))));
end;

procedure TNPPSciFunctions.setSelectionEnd(pPos: Integer);
begin
  SendMessage(getCurrentSciHandle, SCI_SETSELECTIONEND, pPos, 0);
end;

procedure TNPPSciFunctions.setSelectionStart(pPos: Integer);
begin
  SendMessage(getCurrentSciHandle, SCI_SETSELECTIONSTART, pPos, 0);
end;

function TNPPSciFunctions.getLineCount: integer;
begin
  Result := SendMessage(getCurrentSciHandle, SCI_GETLINECOUNT, 0, 0);
end;

function TNPPSciFunctions.getParam;
begin
  Result := SendMessage(getCurrentSciHandle, ParamMSG, Value1, Value2);
end;

function TNPPSciFunctions.setParam(ParamMSG, Value1,
  Value2: integer): integer;
begin
  Result := SendMessage(getCurrentSciHandle, ParamMSG, Value1, Value2);
end;

function TNPPSciFunctions.getNppHandle: HWND;
begin
  Result := FNPPHandle;
end;

function TNPPSciFunctions.GetCaretScreenLocation: TPoint;
var
  pos, x, y: integer;
begin
  pos := getCurrentPos();
  x := getParam(SCI_POINTXFROMPOSITION, 0, pos);
  y := getParam(SCI_POINTYFROMPOSITION, 0, pos);
  Result := Point(x, y);
  ClientToScreen(getCurrentSciHandle, Result);
end;

function TNPPSciFunctions.getCurrentPos: integer;
begin
  Result := SendMessage(getCurrentSciHandle, SCI_GETCURRENTPOS, 0, 0)
end;

function TNPPSciFunctions.TextHeight(line: integer): integer;
begin
  Result := getParam(SCI_TEXTHEIGHT, line);
end;

end.
